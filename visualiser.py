import copy
import tkinter
import time
import math
import random

import pandas as pd
import numpy as np

from util import Color

class NeoPixelVisualiser:

    def __init__(self, height, width, diameter):
        """
        height: number of pixels on y axis
        width: number of pixels on x axis
        these two should match the size of the board you're prototyping for
        diameter: only used for visualisation size here, not relevant for real board
        """
        self.root = tkinter.Tk()
        self.height = height
        self.width = width
        self.diameter = diameter
        self._buffered_state = [Color(0, 0, 0)] * (self.height * self.width)
        self._drawn_objects = []
        canvas_height = (self.height * diameter) * 1.01
        canvas_width = (self.width * diameter) * 1.01
        self.canvas = tkinter.Canvas(self.root, bg="black", height=canvas_height, width=canvas_width)
        # self.canvas = tkinter.Canvas(self.root, bg="#000000", height=500 + (500 * 0.1), width=500 + (500 * 0.1))
        self.canvas.pack()

    def _draw_circle(self, x, y, color):
        r = (self.diameter / 2)
        offset = (r * 0.5)
        x0 = (x - r) + r + offset
        y0 = (y + r) + r + offset
        x1 = (x + r) + r + offset
        y1 = (y - r) + r + offset
        return self.canvas.create_oval(x0, y0, x1, y1, fill=color)

    def _array_to_coords(self, idx):
        y_coord = (self.height - 1) - math.floor(idx / self.width)
        x_coord = idx % self.width
        if math.floor(idx / self.width) % 2:
            # odd row - moves R to L
            x_coord = (self.width - 1) - x_coord

        return (x_coord * self.diameter), (y_coord * self.diameter)

    def _update(self):
        # Work from bottom left to top right alternating right-left-right-left
        [self.canvas.delete(obj) for obj in self._drawn_objects]
        self._drawn_objects = []
        for counter, color in enumerate(self._buffered_state):
            hex_color = hex(color)
            str_hex_color = str(hex_color).replace('0x', '')
            str_hex_color = f'#{str_hex_color.zfill(6)}'
            x, y = self._array_to_coords(counter)
            self._drawn_objects.append(self._draw_circle(x, y, str_hex_color))

    def begin(self):
        self._update()
        self.root.update()

    def show(self):
        self._update()
        self.root.update()
    
    def setPixelColor(self, idx, color):
        if idx > ((self.height * self.width) - 1):
            raise ValueError(f'Requested pixel idx {idx} but only 0-{((self.height * self.width) - 1)} available')
        self._buffered_state[idx] = color

    def setPixelColorRGB(self, n, red, green, blue, white = 0):
            self.setPixelColor(n, Color(red, green, blue))

    def numPixels(self):
        return self.height * self.width


def wheel(pos):
	"""
    Generate rainbow colors across 0-255 positions.
    Taken from https://github.com/jgarff/rpi_ws281x
    """
	if pos < 85:
		return Color(pos * 3, 255 - pos * 3, 0)
	elif pos < 170:
		pos -= 85
		return Color(255 - pos * 3, 0, pos * 3)
	else:
		pos -= 170
		return Color(0, pos * 3, 255 - pos * 3)


def theaterChaseRainbow(strip, wait_ms=50):
	"""
    Rainbow movie theater light style chaser animation.
    Taken from https://github.com/jgarff/rpi_ws281x
    """
	for j in range(256):
		for q in range(3):
			for i in range(0, strip.numPixels(), 3):
				strip.setPixelColor(i+q, wheel((i+j) % 255))
			strip.show()
			time.sleep(wait_ms/1000.0)
			for i in range(0, strip.numPixels(), 3):
				strip.setPixelColor(i+q, 0)


if __name__ == '__main__':
    # rows = [[1, 2, 3], [4, 5, 6]]
    # df = pd.DataFrame(rows)
    # arr = to_strip(df)
    # print(arr)
    viz = NeoPixelVisualiser(16, 21, 20)
    viz.begin()
    theaterChaseRainbow(viz)
    # viz.setPixelColor(0, Color(0, 255, 0))
    # viz.show()
    # viz.root.mainloop()